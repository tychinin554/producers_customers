#include <gtest/gtest.h>

#include <IdInterface.hpp>

#include <array>

TEST(IdInterface_Test, TestInitVal)
{
    IdInterface iI1;
    EXPECT_EQ(iI1.GetId(), 1);
}

TEST(IdInterface_Test, TestOnIncr)
{
    constexpr int arrSize = 10;
    const std::array<IdInterface, arrSize> iIArr;
    const size_t firstId = iIArr[0].GetId();
    for (int i = 0; i < arrSize; i++)
    {
        EXPECT_EQ(iIArr[i].GetId(), firstId + i);
    }
}

TEST(IdInterface_Test, TestOnOutOfScope)
{
    const IdInterface iI1;
    const size_t firstId = iI1.GetId();
    const size_t outOfScopeCount = 3;
    for (size_t i = 0; i < outOfScopeCount; i++)
    {
        IdInterface();
    }
    const IdInterface iT2;
    EXPECT_EQ(iT2.GetId(), 1 + firstId + outOfScopeCount);
}
