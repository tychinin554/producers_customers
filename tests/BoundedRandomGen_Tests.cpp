#include <gtest/gtest.h>

#include <BoundedRandomGen.hpp>

namespace {
    // returns true if val in [lb, ub]
    bool isBetween(int val, int lb, int ub)
    {
        return val >= lb && val <= ub;
    }
}

TEST(BoundedRandomGen_Test, TestCTor)
{
    int lb = -128, ub = 128;
    EXPECT_NO_THROW(BoundedRandomGen bRG(lb, ub));
}

TEST(BoundedRandomGen_Test, TestInvCTor)
{
    int lb = -128, ub = 128;
    EXPECT_THROW(BoundedRandomGen bRG(ub, lb), InvalidSegment);
}

TEST(BoundedRandomGen_Test, TestLBGet)
{
    int lb = -128, ub = 128;
    BoundedRandomGen bRG(lb, ub);
    EXPECT_EQ(lb, bRG.LowerBound());
}

TEST(BoundedRandomGen_Test, TestUBGet)
{
    int lb = -128, ub = 128;
    BoundedRandomGen bRG(lb, ub);
    EXPECT_EQ(ub, bRG.UpperBound());
}

TEST(BoundedRandomGen_Test, TestLBSet)
{
    int lb = -128, ub = 128;
    BoundedRandomGen bRG(lb, ub);
    int newLb = lb - 100;
    bRG.SetLowerBound(newLb);
    EXPECT_EQ(newLb, bRG.LowerBound());
}

TEST(BoundedRandomGen_Test, TestUBSet)
{
    int lb = -128, ub = 128;
    BoundedRandomGen bRG(lb, ub);
    int newUb = ub + 100;
    bRG.SetUpperBound(newUb);
    EXPECT_EQ(newUb, bRG.UpperBound());
}

TEST(BoundedRandomGen_Test, TestInvUBSet)
{
    int lb = -128, ub = 128;
    BoundedRandomGen bRG(lb, ub);
    int newLb = lb - 100;
    EXPECT_THROW(bRG.SetUpperBound(newLb), InvalidSegment);
}

TEST(BoundedRandomGen_Test, TestInvLBSet)
{
    int lb = -128, ub = 128;
    BoundedRandomGen bRG(lb, ub);
    int newUb = ub + 100;
    EXPECT_THROW(bRG.SetLowerBound(newUb), InvalidSegment);
}

TEST(BoundedRandomGen_Test, TestGenOnValidRange)
{
    int lb = -2, ub = 2;
    BoundedRandomGen bRG(lb, ub);
    size_t checksNumber = 1000;
    for (int i = 0; i < checksNumber; i++)
    {
        EXPECT_TRUE(isBetween(bRG.GenRandom(), lb, ub));
    }
}

TEST(BoundedRandomGen_Test, TestGenOnSingleRange)
{
    int lb = 2, ub = 2;
    BoundedRandomGen bRG(lb, ub);
    size_t checksNumber = 1000;
    for (int i = 0; i < checksNumber; i++)
    {
        EXPECT_TRUE(isBetween(bRG.GenRandom(), lb, ub));
    }
}

TEST(BoundedRandomGen_Test, TestGenOnInvalidRange)
{
    int lb = 5, ub = 2;
    EXPECT_THROW(BoundedRandomGen bRG(lb, ub), InvalidSegment);
}

TEST(BoundedRandomGen_Test, TestGenAfterUBSet)
{
    int lb = -128, ub = 128;
    BoundedRandomGen bRG(lb, ub);
    int newUb = ub + 100;
    bRG.SetUpperBound(newUb);
    size_t checksNumber = 1000;
    for (int i = 0; i < checksNumber; i++)
    {
        EXPECT_TRUE(isBetween(bRG.GenRandom(), lb, newUb));
    }
}

TEST(BoundedRandomGen_Test, TestGenAfterLBSet)
{
    int lb = -128, ub = 128;
    BoundedRandomGen bRG(lb, ub);
    int newLb = lb - 100;
    bRG.SetLowerBound(newLb);
    int checksNumber = 1000;
    for (int i = 0; i < checksNumber; i++)
    {
        EXPECT_TRUE(isBetween(bRG.GenRandom(), newLb, ub));
    }
}
