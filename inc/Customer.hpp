#pragma once

#include <IdInterface.hpp>
#include <SharedBuff.hpp>

class Customer final : public IdInterface
{
private:
    SharedBuff& sharedBuff_;

public:
    Customer(SharedBuff& buff);
    
    void Start() const;
};
