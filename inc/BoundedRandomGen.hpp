#pragma once

#include <random>
#include <stdexcept>

// throws when range Lower Bound is bigger than Upper Bound
struct InvalidSegment : std::logic_error
{
    using std::logic_error::logic_error;
};

// Helper class to generate bounded integers
// randomGenerator_.seed() == std::random_device
// MinGW has problems with random_device under GCC 9.2
// @todo add additional preprocessor directive to check MinGW version
// see Notes https://en.cppreference.com/w/cpp/numeric/random/random_device
class BoundedRandomGen final
{
private:
    std::mt19937 randomGenerator_;
    int lowerBound_;
    int upperBound_;

public:
    // may throw InvalidSegment
    BoundedRandomGen(int lowerBound, int upperBound);
    int GenRandom();

    [[nodiscard]] int LowerBound() const noexcept;
    [[nodiscard]] int UpperBound() const noexcept;

    // may throw InvalidSegment
    void SetLowerBound(int lowerBound);
    // may throw InvalidSegment
    void SetUpperBound(int upperBound);
};
