#pragma once

#include <cstddef>

// Inherited by Customer and Producer, so they will have related ids
class IdInterface 
{
private:
    inline static size_t sId_ = 0;

protected:
    size_t myId_;

public:
    IdInterface() : myId_(++sId_) { }
    [[nodiscard]] size_t GetId() const { return myId_; }
};
