#pragma once

#include <SharedBuffRecord.hpp>

#include <condition_variable>
#include <mutex>
#include <optional>
#include <queue>

// Some kind of Channel
class SharedBuff final {
private:
    std::size_t maxLen_;
    std::queue<SharedBuffRecord> buff_;
    bool isClosed_ = false;

    std::mutex mutex_;
    std::condition_variable dataCV_;

public:
    explicit SharedBuff(std::size_t maxLen);

    std::optional<SharedBuffRecord> Next(size_t consumerId);

    void Push(SharedBuffRecord val);

    void Close();
};

