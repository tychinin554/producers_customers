#pragma once

#include <IdInterface.hpp>
#include <SharedBuff.hpp>

// @todo is it better to inherit Producer and Customer from
// BufferUser interface containing sharedBuff_ field?
class Producer final : public IdInterface
{
private:
    SharedBuff& sharedBuff_;
    // counter on produced entites
    size_t myCounter_;

public:
    Producer(SharedBuff& buff);
    
    void Start(size_t numberOfTasks);
};
