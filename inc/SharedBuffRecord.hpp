#pragma once

#include <cstddef>
#include <compare>

// Aggregate representing SharedBuff record
struct SharedBuffRecord
{
    size_t ProducerId;
    size_t ProducerCounter;

    std::strong_ordering operator<=>(const SharedBuffRecord&) const = default;
};
