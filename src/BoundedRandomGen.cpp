#include <BoundedRandomGen.hpp>

// @todo should I throw on contract violation?
BoundedRandomGen::BoundedRandomGen(int lowerBound, int upperBound)
    : randomGenerator_(std::random_device()()), lowerBound_(lowerBound), upperBound_(upperBound)
{
    if (upperBound_ < lowerBound_)
    {
        throw InvalidSegment("Lower bound should be less than upper");
    }
}

int BoundedRandomGen::GenRandom()
{
    std::uniform_int_distribution<int> range(lowerBound_, upperBound_);
    return range(randomGenerator_);
}

int BoundedRandomGen::LowerBound() const noexcept
{
    return lowerBound_; 
}

int BoundedRandomGen::UpperBound() const noexcept
{
    return upperBound_;
}

void BoundedRandomGen::SetLowerBound(int lowerBound)
{
    if (upperBound_ < lowerBound)
    {
        throw InvalidSegment("Lower bound should be less than upper");
    }
    lowerBound_ = lowerBound;
}

void BoundedRandomGen::SetUpperBound(int upperBound)
{
    if (lowerBound_ > upperBound)
    {
        throw InvalidSegment("Upper bound should be greater than lower");
    }
    upperBound_ = upperBound;
}
