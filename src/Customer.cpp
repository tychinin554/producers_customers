#include <Customer.hpp>

#include <BoundedRandomGen.hpp>

#include <thread>

using std::this_thread::sleep_for;
using std::chrono::milliseconds;

namespace
{
    constexpr size_t SLEEPING_LB = 0;
    constexpr size_t SLEEPING_UB = 128;
}

Customer::Customer(SharedBuff& buff)
    : sharedBuff_(buff)
{}

void Customer::Start() const 
{
    BoundedRandomGen rG(SLEEPING_LB, SLEEPING_UB);
    // while (true) is UB
    bool isRunning = true;
    while (isRunning) {
        const std::optional<SharedBuffRecord> producedVal = sharedBuff_.Next(myId_);
        if (!producedVal) {
            isRunning = false;
            continue;
        }
        sleep_for(milliseconds(rG.GenRandom()));
    }
}
