#include <Producer.hpp>
#include <Customer.hpp>

#include <iostream>
#include <queue>
#include <thread>
#include <sstream>

using namespace std::string_literals;

bool cStrToSizeT(const std::string& str, size_t& res)
{
    std::stringstream sstream(str);
    return !(sstream >> res).fail();
}

void printOnCStrToSizeTFail(const std::string& valueName)
{
    std::cerr << "Error: " << '{' << valueName << '}' << " should have unsigned integral type" << std::endl;
}

bool parseArgv(int argc, char* const * const argv, size_t& nProducers, size_t& nCustomers, size_t& queueSize)
{
    bool isValid = true;
    if (argc != 4)
    {
        std::cerr << "Use {producers_number} {customers_number} {queue_size}" << std::endl;
        return false;
    }
    // Here is additional brackets around expression to suppress compiler warning
    if (!((isValid = cStrToSizeT(argv[1], nProducers))))
    {
        printOnCStrToSizeTFail("producers_number"s);
    }
    if (!((isValid = cStrToSizeT(argv[2], nCustomers))))
    {
        printOnCStrToSizeTFail("customers_number"s);
    }
    if (!((isValid = cStrToSizeT(argv[3], queueSize))))
    {
        printOnCStrToSizeTFail("queue_size"s);
    }
    return isValid;
}


// Expected that iteration stops when customers produces queueSize elements
int main(int argc, char** argv)
{
    size_t nProducers, nCustomers, queueSize;
    if (!parseArgv(argc, argv, nProducers, nCustomers, queueSize))
    {
        nProducers = 5;
        nCustomers = 63;
        queueSize = 100;
        std::cout << "Wrong input values, running on default mode" << std::endl;
        std::cout << "{producers_number} = " << nProducers << std::endl;
        std::cout << "{customers_number} = " << nCustomers << std::endl;
        std::cout << "{queue_size} = " << queueSize << std::endl;
    }

    // shared objects
    SharedBuff buff(queueSize);

    // fillig customers and Producers vector
    std::vector<Customer> customers;
    for (size_t i = 0; i < nCustomers; i++)
    {
        customers.emplace_back(buff);
    }
    std::vector<Producer> producers;
    for (size_t i = 0; i < nProducers; i++)
    {
        producers.emplace_back(buff);
    }

    // @todo end criteria not specified by the task, so I've invented it
    const size_t numberOfTasks = queueSize * 2;

    // filling thread vectors
    std::vector<std::thread> vTCons;
    for (size_t i = 0; i < nProducers; i++)
    {
        std::thread custT(&Producer::Start, producers[i], numberOfTasks / nProducers);
        vTCons.push_back(std::move(custT));
    }
    std::vector<std::thread> vTCusts;
    for (size_t i = 0; i < nCustomers; i++)
    {
        std::thread consT(&Customer::Start, customers[i]);
        vTCusts.push_back(std::move(consT));
    }
    
    for (auto& itCons : vTCons)
    {
        itCons.join();
    }
    buff.Close();
    for (auto& itCust : vTCusts)
    {
        itCust.join();
    }
}
