#include <SharedBuff.hpp>

#include <cassert>
#include <iostream>

SharedBuff::SharedBuff(std::size_t maxLen)
    : maxLen_(maxLen)
{
}

std::optional<SharedBuffRecord> SharedBuff::Next(size_t consumerId)
{
    std::unique_lock uLock(mutex_);
    dataCV_.wait(uLock, [this]() {
        return !buff_.empty() || isClosed_;
    });

    if (buff_.empty()) {
        assert(isClosed_);
        return std::nullopt;
    }

    SharedBuffRecord val = buff_.front();
    buff_.pop();
    std::cout << '[' << consumerId << "]: Consumed entity with number = " <<
        val.ProducerCounter << " by producer with id = " <<
        val.ProducerId << std::endl;
    dataCV_.notify_one();
    return val;
}

void SharedBuff::Push(SharedBuffRecord val)
{
    std::unique_lock locker(mutex_);
    dataCV_.wait(locker, [this]() {
        return buff_.size() < maxLen_;
    });

    buff_.push(val);
    dataCV_.notify_one();
}

void SharedBuff::Close()
{
    std::scoped_lock locker(mutex_);
    isClosed_ = true;
    dataCV_.notify_all();
}
