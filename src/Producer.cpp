#include <Producer.hpp>

#include <BoundedRandomGen.hpp>

#include <thread>

using std::this_thread::sleep_for;
using std::chrono::milliseconds;

namespace 
{
    constexpr size_t SLEEPING_LB = 0;
    constexpr size_t SLEEPING_UB = 128;
}

Producer::Producer(SharedBuff& buff)
    : sharedBuff_(buff), myCounter_(0)
{}

void Producer::Start(size_t numberOfTasks)
{
    BoundedRandomGen rG(SLEEPING_LB, SLEEPING_UB);
    for (size_t i = 0; i < numberOfTasks; i++)
    {
        myCounter_++;
        sharedBuff_.Push({myId_, myCounter_});
        sleep_for(std::chrono::milliseconds(rG.GenRandom()));
    }
}
