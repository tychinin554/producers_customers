## Конфигурирование gitlab ci/cd  

> Настройка https://www.youtube.com/watch?v=R58OuSts948&t=401s&ab_channel=DeusOps
> Windows gitlab runner https://habr.com/ru/company/rostelecom/blog/546702/#gitlab_runner_install

Для сборки solution Visual Studio используется команда msbuild.  

### Настройка gitlab ci:  

Установка и регистрация gitlab runner:
1. Скачать https://docs.gitlab.com/runner/install/windows.html и поместить в папку;  

1. Скачать gitlab runner
2. gitlab-runner start
3. gitlab-runner register (token placed on gitlab web)
4. Для корректной сборки должны использоваться теги при регистрации раннеров
